-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2020 at 06:45 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `payroll_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `firstname`, `lastname`, `photo`, `created_on`) VALUES
(1, 'admin', '$2y$10$KClKG9BpkKK432wptCgVWOfll.5PMyCP2CwrcA4GjVoI7Wu6Rla/W', 'Harry', 'Den', 'male6.jpg', '2018-04-30');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time_in` time NOT NULL,
  `status` int(1) NOT NULL,
  `time_out` time DEFAULT NULL,
  `num_hr` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `employee_id`, `date`, `time_in`, `status`, `time_out`, `num_hr`) VALUES
(114, 15103287, '2019-11-20', '11:00:00', 0, '14:00:00', 3),
(115, 15103287, '2019-11-01', '08:00:00', 1, '17:00:00', 8),
(116, 15103287, '2019-11-02', '08:00:00', 1, '17:00:00', 8),
(137, 16103383, '2019-11-24', '12:09:14', 0, NULL, NULL),
(138, 16103382, '2019-11-27', '16:12:58', 0, NULL, NULL),
(139, 16103382, '2019-12-04', '13:50:48', 0, '23:07:10', 8.2666666666667),
(140, 16103380, '2019-12-04', '13:52:27', 0, '23:07:23', 8.2333333333333),
(143, 16103371, '2019-12-12', '12:31:55', 0, '21:06:57', 7.5833333333333),
(148, 15103287, '2019-12-30', '09:42:46', 1, '18:52:01', 7.8666666666667),
(149, 16103370, '2019-12-30', '09:45:18', 1, '18:48:24', 7.8),
(150, 16103371, '2019-12-30', '09:45:47', 1, '18:48:14', 6.8),
(151, 16103372, '2019-12-30', '09:49:30', 1, '18:48:39', 7.8),
(152, 16103381, '2019-12-30', '09:52:58', 1, '18:49:51', 7.8166666666667),
(155, 16103371, '2020-01-01', '12:45:54', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cashadvance`
--

CREATE TABLE `cashadvance` (
  `id` int(11) NOT NULL,
  `date_advance` date NOT NULL,
  `employee_id` varchar(15) NOT NULL,
  `amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cashadvance`
--

INSERT INTO `cashadvance` (`id`, `date_advance`, `employee_id`, `amount`) VALUES
(11, '2019-12-15', '16103380', 600),
(13, '2019-12-17', '16103382', 700),
(15, '2019-12-30', '16103372', 200),
(16, '2019-12-31', '16103371', 500);

-- --------------------------------------------------------

--
-- Table structure for table `deductions`
--

CREATE TABLE `deductions` (
  `description` varchar(100) NOT NULL,
  `amount` double NOT NULL,
  `date` varchar(30) NOT NULL,
  `id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deductions`
--

INSERT INTO `deductions` (`description`, `amount`, `date`, `id`) VALUES
('16103383', 500, '2019-12-15', 7),
('16103380', 600, '2019-12-15', 8),
('16103371', 500, '01.01.2020', 10),
('16103372', 700, '01.01.2020', 11);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(15) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `birthdate` date NOT NULL,
  `contact_info` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `position_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `employee_id`, `firstname`, `lastname`, `address`, `birthdate`, `contact_info`, `gender`, `position_id`, `schedule_id`, `photo`, `created_on`) VALUES
(26, '15103287', 'jannatul', 'adnan', 'rangpur', '1992-05-02', '01866566565', 'Female', 2, 7, 'images (2).jpg', '2019-06-30'),
(27, '16103045', 'Sojib', 'hossain', 'Tangail', '1997-02-07', '01735656565', 'Male', 1, 4, 'facebook-profile-image.jpeg', '2019-07-20'),
(29, '16103382', 'Lutfun ', 'Nahar', 'Banani', '1992-11-05', '01785962148', 'Female', 3, 2, 'images.jpg', '2019-11-24'),
(30, '16103381', 'Kulsum', 'Akter', 'Uttara', '1990-05-05', '01658974569', 'Female', 4, 4, '145847.png', '2019-11-24'),
(31, '16103380', 'Razia ', 'Sultana', 'Badda', '1993-01-26', '01745896231', 'Female', 3, 3, 'images (3).jpg', '2019-11-27'),
(32, '16103370', 'Abdul', 'Karim', 'mnj', '1990-10-25', '01658741258', 'Male', 4, 4, 'Extension_06_Mahbubul_Alam.jpg', '2019-11-27'),
(33, '16103371', 'Humayun ', 'Kabir', 'Gazipur', '1988-12-25', '01987546231', 'Male', 5, 8, 'Humayun.jpg', '2019-12-05'),
(34, '16103384', 'Rokeya', 'Akter', 'AFDA', '2019-12-18', 'AFA', 'Female', 6, 4, 'toma apu (2).jpg', '2019-12-26'),
(35, '16103372', 'Haider ', 'Ali', 'Mirpur', '1990-05-07', '01478963285', 'Male', 8, 7, 's200_muhammad_humayun.kabir.jpg', '2019-12-28');

-- --------------------------------------------------------

--
-- Table structure for table `employee_login`
--

CREATE TABLE `employee_login` (
  `id` int(20) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_login`
--

INSERT INTO `employee_login` (`id`, `user_id`, `password`) VALUES
(1, '15103272', '12345'),
(2, '16103383', '3383'),
(3, '16103382', '3382'),
(4, '16103381', '3381'),
(5, '16103380', '3380'),
(6, '16103371', '3371'),
(7, '16103372', '3372'),
(8, '16103373', '3373'),
(9, '16103374', '3374'),
(10, '16103375', '3375');

-- --------------------------------------------------------

--
-- Table structure for table `emp_leave`
--

CREATE TABLE `emp_leave` (
  `id` int(20) NOT NULL,
  `empid` varchar(50) NOT NULL,
  `from` varchar(100) NOT NULL,
  `to` varchar(50) NOT NULL,
  `reason` text NOT NULL,
  `status` int(50) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_leave`
--

INSERT INTO `emp_leave` (`id`, `empid`, `from`, `to`, `reason`, `status`, `name`) VALUES
(24, '16103383', '2019-12-24', '2019-12-26', ' marriage ceremony', 3, 'Zerin'),
(25, '16103383', '2019-05-25', '2019-05-27', ' sickness', 2, 'Zerin'),
(26, '16103380', '2019-12-05', '2019-12-07', ' sickness', 1, 'Razia '),
(27, '16103382', '2019-10-10', '2019-10-12', ' elder sister marriage\r\n', 0, 'Lutfun '),
(28, '16103381', '2019-08-10', '2019-08-13', ' sickness', 1, 'Kulsum'),
(31, '16103383', '2019-12-30', '2020-01-02', ' hbgvtgf', 0, 'Zerin'),
(33, '16103383', '2020-01-01', '2019-12-04', ' sick', 0, 'Zerin'),
(34, '16103382', '2019-12-31', '2020-01-03', ' sick', 1, 'Lutfun '),
(35, '16103380', '2020-01-01', '2020-01-03', ' sick\r\n', 1, 'Razia ');

-- --------------------------------------------------------

--
-- Table structure for table `emp_monitoring`
--

CREATE TABLE `emp_monitoring` (
  `id` int(20) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `running_project` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `points` int(20) NOT NULL,
  `start_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `feedback` text NOT NULL,
  `submit_date` varchar(20) NOT NULL,
  `emp_commends` text NOT NULL,
  `file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_monitoring`
--

INSERT INTO `emp_monitoring` (`id`, `emp_id`, `name`, `position`, `running_project`, `status`, `points`, `start_date`, `end_date`, `feedback`, `submit_date`, `emp_commends`, `file`) VALUES
(10, '16103381', 'Kulsum', 'programmer', 'ecommerce project', '0', 0, '2019-10-10', '2019-10-25', '', '', '', ''),
(12, '16103382', 'Lutfun ', 'programmer', 'Office management', '0', 0, '2019-11-25', '2019-12-05', '', '', '', ''),
(14, '16103380', 'Razia ', 'programmer', 'Online Shopping Management', '0', 0, '2019-12-27', '2019-12-31', '', '', '', ''),
(15, '16103370', 'Abdul', 'programmer', 'Hospital Management', '0', 0, '2019-12-15', '2019-12-20', '', '', '', ''),
(16, '16103380', 'Razia ', 'programmer', 'library management', '0', 0, '2019-12-31', '2020-01-02', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `emp_ranking`
--

CREATE TABLE `emp_ranking` (
  `id` int(20) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rank` int(20) NOT NULL,
  `points` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(20) NOT NULL,
  `notice_title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(15) NOT NULL,
  `hours` double NOT NULL,
  `rate` double NOT NULL,
  `date_overtime` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overtime`
--

INSERT INTO `overtime` (`id`, `employee_id`, `hours`, `rate`, `date_overtime`) VALUES
(3, '16103382', 2, 300, '2019-12-12'),
(4, '16103372', 3, 150, '2019-12-17'),
(5, '16103372', 2.5, 300, '2019-12-19'),
(7, '16103382', 1, 150, '2019-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `id` int(11) NOT NULL,
  `description` varchar(150) NOT NULL,
  `rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `description`, `rate`) VALUES
(1, 'Programmer', 100),
(2, 'Writer', 50),
(3, 'Marketing ', 60),
(4, 'Graphic Designer', 90),
(5, 'Senior Programmar', 200),
(6, 'Web Developer', 120),
(7, 'Junior Programmar', 150),
(8, 'junior Enginner', 120);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `time_in`, `time_out`) VALUES
(2, '08:00:00', '17:00:00'),
(3, '09:00:00', '18:00:00'),
(4, '10:00:00', '19:00:00'),
(5, '08:00:00', '16:00:00'),
(6, '09:00:00', '17:00:00'),
(7, '10:00:00', '18:00:00'),
(8, '11:00:00', '03:00:00'),
(15103287, '07:00:00', '16:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashadvance`
--
ALTER TABLE `cashadvance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deductions`
--
ALTER TABLE `deductions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_login`
--
ALTER TABLE `employee_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_leave`
--
ALTER TABLE `emp_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_monitoring`
--
ALTER TABLE `emp_monitoring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_ranking`
--
ALTER TABLE `emp_ranking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `cashadvance`
--
ALTER TABLE `cashadvance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `deductions`
--
ALTER TABLE `deductions`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `employee_login`
--
ALTER TABLE `employee_login`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `emp_leave`
--
ALTER TABLE `emp_leave`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `emp_monitoring`
--
ALTER TABLE `emp_monitoring`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `emp_ranking`
--
ALTER TABLE `emp_ranking`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15103288;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
