<?php

	include 'includes/session.php';

	function generateRow($from, $to, $conn){
		$contents = '';
	 	
		$sql = "SELECT *, sum(num_hr) AS total_hr, attendance.employee_id AS empid FROM attendance ";
		$sql.= "LEFT JOIN employees ON employees.employee_id=attendance.employee_id ";
		$sql.= "LEFT JOIN position ON position.id=employees.position_id ";
		$sql.= "WHERE date BETWEEN '$from' AND '$to' GROUP BY attendance.employee_id ";
		$sql.= "ORDER BY employees.lastname ASC, employees.firstname ASC";
		$query = $conn->query($sql);
		$total = 0;
		while($row = $query->fetch_assoc()){
			$empid = $row['empid'];
                      
	      	$casql = "SELECT *, SUM(amount) AS cashamount FROM cashadvance WHERE employee_id='$empid' AND date_advance BETWEEN '$from' AND '$to'";
	      	$caquery = $conn->query($casql);
	      	$carow = $caquery->fetch_assoc();
	      	$cashadvance = $carow['cashamount'];
			
			$deductionsql = "SELECT *, SUM(amount) as total_amount FROM deductions WHERE description = '$empid' ";
			$deductionquery = $conn->query($deductionsql);
			$drow = $deductionquery->fetch_assoc();
			$deduction = $drow['total_amount'];
			
			$overtime = 0;
			$otsql = "SELECT * FROM overtime WHERE employee_id='$empid' AND date_overtime BETWEEN '$from' AND '$to'";
			$otquery = $conn->query($otsql);
			while($r_ot = $otquery->fetch_assoc()) $overtime += $r_ot['hours']*$r_ot['rate'];
			
			
			$gross = 15000+ $row['rate'] * $row['total_hr'];
			$total_deduction = $deduction + $cashadvance;
      		$net = $gross + $overtime - $total_deduction;
			
			$total += $net;
			$contents .= '
			<tr>
				<td>'.$row['firstname'].' '.$row['lastname'].'</td>
				<td>'.$row['employee_id'].'</td>
				<td align="right">'.number_format($net, 2).'</td>
			</tr>';
		}

		$contents .= '
			<tr>
				<td colspan="2" align="right"><b>Total</b></td>
				<td align="right"><b>'.number_format($total, 2).'</b></td>
			</tr>
		';
		return $contents;
	}
		
	$range = $_POST['date_range'];
	$ex = explode(' - ', $range);
	$from = date('Y-m-d', strtotime($ex[0]));
	$to = date('Y-m-d', strtotime($ex[1]));


	$from_title = date('M d, Y', strtotime($ex[0]));
	$to_title = date('M d, Y', strtotime($ex[1]));
	
    $content = '';  
    $content .= '
      	<h2 align="center">Payroll Management System</h2>
      	<h4 align="center">'.$from_title." - ".$to_title.'</h4>
      	<table border="1" cellspacing="0" cellpadding="3">  
           <tr>  
           		<th width="40%" align="center"><b>Employee Name</b></th>
                <th width="30%" align="center"><b>Employee ID</b></th>
				<th width="30%" align="center"><b>Net Pay</b></th> 
           </tr>  
      ';  
    $content .= generateRow($from, $to, $conn);  
    $content .= '</table>';
	echo "<center>".$content."</center>";
    //$pdf->writeHTML($content);  
    //$pdf->Output('payroll.pdf', 'I');

?>